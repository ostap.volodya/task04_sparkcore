package com.ostapiuk;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Serializable;
import java.util.List;

/*
 * Joining two RDD's using the Spark Java API
 */
public class AwardsCounter {
    private static class Player implements Serializable {
        private String firstName;
        private String lastName;
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String name) {
            firstName = name;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String name) {
            lastName = name;
        }
    }

    public static class PlayerAwards implements Serializable {
        private String id;
        private String award;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        String getAward() {
            return award;
        }

        void setAward(String newAward) {
            award = newAward;
        }
    }

    public static void main(String[] args) throws IOException {
        SparkSession sparkSession = SparkSession
                .builder()
                .master("local")
                .appName("PlayerAwardsCount")
                .getOrCreate();

        JavaRDD<String> awardsRDD = createRDD(sparkSession, "src/main/resources/AwardsPlayers.csv");
        JavaRDD<String> playersRDD = createRDD(sparkSession, "src/main/resources/Master.csv");

        JavaPairRDD<String, Tuple2<PlayerAwards, Player>> joinedRDD = joinRDD(awardsRDD, playersRDD);

        printResult(transformRDD(joinedRDD));

        sparkSession.stop();
    }

    private static JavaRDD<String> createRDD(SparkSession sparkSession, String path) {
        return sparkSession
                .read()
                .textFile(new File(path).getAbsolutePath())
                .javaRDD();
    }

    /*
     *  Produces new RDD from the existing RDDs
     */
    private static JavaPairRDD<String, Tuple2<PlayerAwards, Player>> joinRDD(JavaRDD<String> awardsRDD, JavaRDD<String> playersRDD) {
        //  join - When called on datasets of type (K, V) and (K, W), returns a dataset of
        // (K, (V, W)) pairs with all pairs of elements for each key.
        return mapAwardRDDToPair(awardsRDD).join(mapPlayerRDDToPair(playersRDD));
    }

    private static JavaPairRDD<String, PlayerAwards> mapAwardRDDToPair(JavaRDD<String> awardsRDD) {
        //Tuple - get multiply result from function
        return awardsRDD.mapToPair((PairFunction<String, String, PlayerAwards>) s -> {
            String[] words = s.split(",");
            int award = 1;
            PlayerAwards playerAwards = new PlayerAwards();
            playerAwards.setId(words[0]);
            playerAwards.setAward(Integer.toString(award));
            return new Tuple2<>(words[0], playerAwards);
        });
    }

    private static JavaPairRDD<String, Player> mapPlayerRDDToPair(JavaRDD<String> playersRDD) {
        return playersRDD.mapToPair((PairFunction<String, String, Player>) s -> {
            String[] words = s.split(",");
            Player player = new Player();
            player.setId(words[0]);
            player.setFirstName(words[3]);
            player.setLastName(words[4]);
            return new Tuple2<>(words[0], player);
        });
    }

    /*
     * Makes wide transformations
     */
    private static JavaPairRDD<String, Tuple2<PlayerAwards, Player>> transformRDD(JavaPairRDD<String, Tuple2<PlayerAwards, Player>> joinedRDD) {
        // Reduce it by id
        return joinedRDD.reduceByKey((Function2<Tuple2<PlayerAwards, Player>, Tuple2<PlayerAwards, Player>, Tuple2<PlayerAwards, Player>>) (v1, v2) -> {
            PlayerAwards playerAwards = new PlayerAwards();
            int total = Integer.parseInt(v1._1().getAward()) +
                    Integer.parseInt(v2._1().getAward());
            playerAwards.setAward(Integer.toString(total));
            playerAwards.setId(v1._1().getId());
            return new Tuple2<PlayerAwards, Player>(playerAwards, v1._2());
        });
    }

    /*
     * Prints result of RDD transformation to the file
     *
     * @throws IOException
     */
    private static void printResult(JavaPairRDD<String, Tuple2<PlayerAwards, Player>> transformedResult) throws IOException {
        JavaRDD<String> countedPlayersAwards = transformedResult.map(x -> {
            Tuple2<PlayerAwards, Player> tuple2 = x._2();
            PlayerAwards playerAwards = tuple2._1();
            Player player = tuple2._2();
            return "Player - " + player.getFirstName() + " "
                    + player.getLastName() + ", his id: " + x._1()
                    + ", number of awards -> " + playerAwards.getAward();
        });
        countedPlayersAwards.coalesce(1).saveAsTextFile("src/main/resources/output.txt");
        countedPlayersAwards.coalesce(1).saveAsTextFile("src/main/resources/output.csv");
    }
}
